import React, { useContext, useState } from "react";
import { AppContext } from "../auth/context";
import { AddEnergyDrinkForm } from "../components/AddEnergyDrinkForm";
import EnergyDrinkTable from "../components/EnergyDrinkTable";

export const EnergyDrinkScreen = () => {
  //For at kunne logge en user ud skal jeg bruge, context og
  //- tomme værdier for username og password
  const context = useContext(AppContext);

  const emtyUsername = "";
  const emtyPassword = "";

  const [energyDrinks, setEnergyDrinks] = useState<EnergyDrink[]>([]);
  //Callback for adding an energydrink
  const addDrink = (name: string) => {
    //Tilføjer et random id til oprettelse af ny energydrink
    const min = 1;
    const max = 100;
    const rand = min + Math.random() * (max - min);
    const newdrink = { name: name, id: rand };

    setEnergyDrinks([...energyDrinks, newdrink]);
  };

  //Slettet den energydrink
  const deleteDrink = (name: string) => {
    setEnergyDrinks((prev) =>
      prev.filter((energyDrink) => energyDrink.name !== name)
    );
  };

  return (
    <div className="container">
      <h1>List of Energydrinks</h1>
      <div className="flex-row">
        <div className="flex-large">
          <EnergyDrinkTable
            drinks={energyDrinks}
            deleteEnergyDrink={deleteDrink}
          />
          <AddEnergyDrinkForm onSubmit={addDrink} />
        </div>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            context.signOut(emtyUsername, emtyPassword);
          }}
        >
          <button type="submit">Logout</button>
        </form>
      </div>
    </div>
  );
};
