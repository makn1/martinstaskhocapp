import React, { useContext, useState } from "react";
import { AppContext } from "../auth/context";
export const LoginScreen = () => {
  //For at kunne logge en bruger ind skal vi bruge context
  const context = useContext(AppContext);

  const [usernameValue, setUserNameValue] = useState("");
  const [passwordValue, setPasswordValue] = useState("");

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        context.signIn(usernameValue, passwordValue);
      }}
    >
      <label>Username</label>
      <input
        type="text"
        value={usernameValue}
        onChange={(e) => setUserNameValue(e.target.value)}
      />
      <div>
        {" "}
        <label>Password</label>
        <input
          type="text"
          value={passwordValue}
          onChange={(e) => setPasswordValue(e.target.value)}
        />
      </div>
      <button type="submit">Login</button>
    </form>
  );
};
