import React, { FC, useState } from "react";

//Props jeg vil bruge i min context
type ContextProps = {
  user: {
    name: string;
    password: string;
  };
  signIn: (user: string, pswd: string) => void;
  signOut: (user: string, pswd: string) => void;
  auth: boolean;
};

//@ts-ignore
export const AppContext = React.createContext<ContextProps>(undefined);

//Opretter min egen custom AppContext
export const UserProvider: FC = ({ children }) => {
  const [user, setUser] = useState({ name: "", password: "" });
  const [auth, setAuth] = useState(false);

  //Logger user ind med hardcoded values og giver error hvis det er forkert
  const signIn = (user: string, password: string) => {
    if (user === "A" && password === "A") {
      setUser({ name: user, password: password });
      setAuth(true);
    } else alert("Wrong username or password, please try again");
  };

  //Logger brugeren ud
  const signOut = (user: string, password: string) => {
    setUser({ name: "", password: "" });
    setAuth(false);
  };

  //De props jeg asigner min AppContext
  const defaultUser: ContextProps = {
    user: user,
    signIn: signIn,
    signOut: signOut,
    auth: auth,
  };

  return (
    <AppContext.Provider value={defaultUser}>{children}</AppContext.Provider>
  );
};
