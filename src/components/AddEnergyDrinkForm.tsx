import React, { useState } from "react";

interface SubmitProps {
  onSubmit: (name: string) => void;
}

//Tilføjer en energydrink
export const AddEnergyDrinkForm: React.FC<SubmitProps> = (props) => {
  const [text, setText] = useState("");

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    props.onSubmit(text);
    setText("");
  };

  const handleTextChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setText(e.target.value);
  };

  return (
    <form onSubmit={handleSubmit}>
      <input type="text" value={text} onChange={handleTextChange} />
      <button type="submit">Add Energydrink</button>
    </form>
  );
};
