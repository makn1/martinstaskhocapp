import React from "react";

//Viser alle energydrinks der ligger i EnergyDrink[]

//Interface til at sikre jeg får de værdier jeg ønsker og skal bruge
interface TableProps {
  drinks: EnergyDrink[];
  deleteEnergyDrink: (name: string) => void;
}

//Vister table af energydrinks
//Her er der også mulighed for at slette en energydrink
const EnergyDrinkTable: React.FC<TableProps> = (props) => (
  <table>
    <thead>
      <tr>
        <th>Name of Energydrink</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      {props.drinks.map((drink) => (
        <tr key={drink.name}>
          <td>{drink.name}</td>
          <td>
            <button
              className="button muted-button"
              onClick={() => props.deleteEnergyDrink(drink.name)}
            >
              Delete
            </button>
          </td>
        </tr>
      ))}
    </tbody>
  </table>
);

export default EnergyDrinkTable;
