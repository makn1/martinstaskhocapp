import "./App.css";
import { useContext } from "react";
import { EnergyDrinkScreen } from "./screens/EnergyDrinksScreen";
import { LoginScreen } from "./screens/LoginScreen";
import { Switch, Route } from "react-router-dom";
import { AppContext } from "./auth/context";

//Sender brugeren til login screen hvis de ikke er logget ind (auth: true)
const WhereToGo = () => {
  const authContext = useContext(AppContext);

  if (authContext.auth) {
    return (
      <Switch>
        <Route exact path="/" component={EnergyDrinkScreen} />
      </Switch>
    );
  }
  return <LoginScreen />;
};

const App = () => {
  return (
    <div className="container">
      <div className="flex-row">
        <div className="flex-large">
          <WhereToGo />
        </div>
      </div>
    </div>
  );
};

export default App;
